There isn't an API, this time, to get data from the timeline, but the data can be access from takeout by the user. So the only thing you can do is request from the user to upload the JSON(or KML) file of the data.

You can put in your site a url like this: https://takeout.google.com/settings/takeout/custom/location_history and tell to user to download the json file and upload it.
